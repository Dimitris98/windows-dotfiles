#!/bin/bash

# Shell Script for my WSL Devbox.

apps=(
	# docker
	# docker-compose
	aria2
	bat
	cmake
	crun
	default-jdk
	duf
	eza
	fd-find
	fzf
	git
	htop
	neofetch
	neovim
	podman
	podman-compose
	podman-docker
	ranger
	sqlite3
	thefuck
	tldr
	tmux
	tree
	zoxide
	zsh)

systemUpdate() {

	# Full system update.

	sudo apt update && sudo apt full-upgrade -y

	# Install apps from list.

	echo "Install apps..."
	sudo apt install "${apps[@]}" -y

	# Add user to Docker group and enable the daemon.

	# sudo usermod -aG docker $(whoami)
	# sudo systemctl start docker

	# Enable Podman socket service to allow API communication without requiring root privileges.
    systemctl --user enable podman.socket || {
    	echo "Failed to enable Podman socket. Ensure 'podman' is installed correctly.";
        return 1;
    }

	# Install Nix via the single-user instalation.

	sh <(curl -L https://nixos.org/nix/install) --no-daemon

	# Download and install oh-my-zsh.

	echo "Install oh-my-zsh"

	sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended

	# Download zsh plugins.

	echo "Download & install zsh plugins"

	# Powerlevel10k plugin.
	git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
	# Autosuggestions plugin.
	git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
	# Autocomplete plugin.
	cd /home/$(whoami)/.oh-my-zsh/plugins
	git clone --depth 1 -- https://github.com/marlonrichert/zsh-autocomplete.git
	# Highlight plugin.
	git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
	# Complition plugin.
	git clone https://github.com/zsh-users/zsh-completions ${ZSH_CUSTOM:-${ZSH:-~/.oh-my-zsh}/custom}/plugins/zsh-completions
	cd ~/
	# Make zsh the default shell.
	chsh -s $(which zsh)

	# Install AstroNvim.
	git clone --depth 1 https://github.com/AstroNvim/template ~/.config/nvim
	rm -rf ~/.config/nvim/.git
	nvim

}

echo "
__        ______  _
\ \      / / ___|| |
 \ \ /\ / /\___ \| |
  \ V  V /  ___) | |___
   \_/\_/  |____/|_____|
"
BOLD=$(tput bold)

echo -e "${BOLD}Welcome!"

systemUpdate
