# PowerShell script for my freshly formatted Windows machines. The script utilize the Windows built-in package manager winget to install / update
# all of my default and new applications and also copy my personal files from my external drive.

# Install my default applications using winget.
function Install-Default-Apps {
    $packages = @(#"BlueStack.BlueStacks",
    #"GIMP.GIMP",
    #"Henry++.simplewall",
    #"KDE.Kdenlive",
    #"Microsoft.WindowsTerminal", Preinstalled in Windows 11
    #"Nvidia.GeForceExperience",
    #"Oracle.VirtualBox",
    #"WiresharkFoundation.Wireshark"
    "7zip.7zip",
    "Audacity.Audacity",
    "Discord.Discord",
    "ElementLabs.LMStudio",
    "Elgato.StreamDeck",
    "HeroicGamesLauncher.HeroicGamesLauncher",
    "JanDeDobbeleer.OhMyPosh",
    "Jellyfin.Server",
    "Libretro.RetroArch",
    "LocalSend.LocalSend",
    "Microsoft.PowerShell",
    "Microsoft.PowerToys",
    "Mozilla.Firefox",
    "Notepad++.Notepad++",
    "Oracle.JavaRuntimeEnvironment",
    "Playnite.Playnite",
    "PrismLauncher.PrismLauncher",
    "Python.Python.3.13",
    "qBittorrent.qBittorrent",
    "Safing.Portmaster",
    "ShareX.ShareX",
    "Spotify.Spotify",
    "stnkl.EverythingToolbar",
    "SumatraPDF.SumatraPDF",
    "TheDocumentFoundation.LibreOffice",
    "Upscayl.Upscayl",
    "Valve.Steam",
    "VB-Audio.Voicemeeter.Banana",
    "VideoLAN.VLC",
    "voidtools.Everything",
    "VSCodium.VSCodium")

    foreach ($package in $packages) {
        winget install --exact --id $package --source winget
    }

}

# Install new apps using winget.
function Install-New-Apps {
    do {
        Write-Host "Enter package name you want to install with winget" -ForegroundColor Yellow
        $appName = Read-Host 
        $searchApp = winget search --name-match $appName

        if ($null -ne $searchApp -and $appName) {
            winget install --exact --id $appName --source winget
            break
        }
        elseif ($appName) {
            Write-Host "Package $appName was not found!" -ForegroundColor Red
        }
    } while ($true)
}

# Update preinstalled apps using winget.
function Update-Apps {
    winget upgrade --all
}

# Edit winget settings.
function Update-Settings {
    winget settings
}

# Install PowerShell Modules.
function PS-Modules {

    # Fzf Module.
    Install-Module -Name PSFzf

    # Terminal-Icons Module.
    Install-Module -Name Terminal-Icons -Repository PSGallery

}

# Remove Microsoft Store from winget source.
function Remove-Store {
    winget source remove msstore
}

# Install HEVC Codecs from Microsoft Store.
function HEVC-Codecs {
    $url = "ms-windows-store://pdp/?ProductId=9n4wgh0z6vhq"
    Start-Process $url
}

# Install Distros via WSL.
function WSL-Setup {
    wsl --update
    wsl --shutdown
    wsl --install -d Ubuntu-24.04
}

# Restore personal folders from external drive to main drive using restic.
function Restic-Backup {

    $USER = $env:username
    $script = "C:\Users\$USER\Documents\Powershell_Scripts\backup.ps1"

    if (Test-Path script){
        & $script
    } else {
        Write-Error "Script not found!" -ForegroundColor red
    }
}

# Chris Titus Tech's Windows Utility for installing apps and debloat Windows.
function DebloatWindows {
    $url = "https://christitus.com/win"

    Write-Host "Downloading and running debloat script from: $url"
    try {
        irm $url | iex
    }
    catch {
        Write-Error "An error occurred."
    }
}

# TUI Layout.
do {
    Clear-Host
    Write-Host "__        ___           _                   "
    Write-Host "\ \      / (_)_ __   __| | _____      _____ "
    Write-Host " \ \ /\ / /| | '_ \ / _` |/ _ \ \ /\ / / __|"
    Write-Host "  \ V  V / | | | | | (_| | (_) \ V  V /\__ \"
    Write-Host "   \_/\_/  |_|_| |_|\__,_|\___/ \_/\_/ |___/"
    Write-Host "Welcome to Windows Setup Script!"
    Write-Host "Disclaimer: Please run the script as administrator if you want to use option 5, or 6!" -ForegroundColor Yellow
    Write-Host "Please select an option:"
    Write-Host "1) Install default applications."
    Write-Host "2) Install new applications."
    Write-Host "3) Update installed Apps."
    Write-Host "4) Edit winget settings."
    Write-Host "5) Install PowerShell Modules."
    Write-Host "6) Remove Microsoft Store for winget source."
    Write-Host "7) Install HEVC Codecs."
    Write-Host "8) Install Ubuntu via WSL."
    Write-Host "9) Restore backup folders from external drive to main drive (Requires restic to be installed)."
    Write-Host "10) Run debloat script."
    Write-Host "0) Exit."
    $choice = Read-Host

    switch ($choice) {
        "1" {Install-Default-Apps}
        "2" {Install-New-Apps}
        "3" {Update-Apps}
        "4" {Update-Settings}
        "5" {PS-Modules}
        "6" {Remove-Store}
        "7" {HEVC-Codecs}
        "8" {WSL-Setup}
        "9" {Restic-Backup}
        "10" {DebloatWindows}
        "0" {
            Write-Host "Exiting the script." -ForegroundColor Green
        }
        default {
            Write-Host "Invalid choice! Please try again." -ForegroundColor Red
        }
    }

} while ($choice -ne "0")
