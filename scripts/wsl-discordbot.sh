#!/bin/bash

# Full system update and installing the pre-requirements.
sudo apt update && sudo apt -y full-upgrade
sudo apt -y install build-essential git openjdk-17-jre-headless python3.11 python3.11-dev python3.11-venv

# Create virtual environment using Venv.
python3.11 -m venv ~/redenv
source ~/redenv/bin/activate

# Installing Red without additional config backend support.
python -m pip install -U pip wheel
python -m pip install -U Red-DiscordBot

# Setting Up and Running Red.
redbot-setup
# redbot <your instance name>
