# PowerShell script to resolve the issue of crackling audio on Discord while using Voicemeeter.

$Process = Get-Process audiodg
$Process.PriorityClass = [System.Diagnostics.ProcessPriorityClass]::High
$Process.ProcessorAffinity = [Int64] 2
