# PowerShell script to automate the task of backup using restic.

# Retrieve Username.

$USER = $env:username

# Restic.exe PATH.

$PATH = "C:\Users\$USER\Documents\Terminal_Apps\restic\restic.exe"

# Backup folder PATH (External Drive).

$repo = "D:\Restic_Backup"

# Password txt PATH.

$password = "C:\Users\$USER\Documents\Powershell_Scripts\restic.txt"

# Folders to use for backup.

$folders = "C:\Users\$USER\Documents C:\Users\$USER\Pictures C:\Users\$USER\Videos C:\Users\$USER\Music"

# Backup command.

$command = "$PATH -r $repo backup $folders --password-file $password"

Invoke-Expression $command
