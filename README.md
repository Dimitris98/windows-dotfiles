<h1 align=center>🪟 Windows Dotfiles 🗃️</h1>

```{.bash}
git clone https://gitlab.com/Dimitris98/windows-dotfiles.git
```

## PowerShell Settings ⚙️

To run the PowerShell scripts, you must set the execution policy to **Unrestricted** for the current process. First, check the current policy by typing the following command in your terminal:

```{.bash}
Get-ExecutionPolicy
```

If the policy is not already set to Unrestricted or Bypass, update it using the following command:

```{.bash}
Set-ExecutionPolicy Unrestricted -Scope Process
```
This will set the policy only for the duration of the current PowerShell session and does not require administrator privileges.

To disable the **personal and system profiles loading time** banner from PowerShell you can add the following option to the PowerShell installation path

```{bash}
"Path_of_PowerShell\pwsh.exe" -nologo
```

To set up the PowerShell profile for the latest version and the preinstalled version, you simply need to create a folder called **PowerShell** (_for the latest version_) and a folder called **WindowsPowerShell** (_for the preinstalled version_), and move the file to both of them. The same goes for the Powershell modules, you have to install them twice for each version of Powershell.

To edit the preinstalled PowerShell, you need to right click on the top left icon and then go to properties, from there you can pretty much edit the whole thing as you like.

To make the restic backup and the voicemeeter-fix script (_requires administrator privileges_) run each time Windows starts up, use the **Windows Task Scheduler**.

## Terminal Tools 🧰

The list of Terminal applications I use daily

| Application Name                                                                      | Description                                                                       |
| ------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------- |
| [bat](https://github.com/sharkdp/bat)                                                 | A cat(1) clone with wings                                                         |
| [fd](https://github.com/sharkdp/fd)                                                   | A simple, fast and user-friendly alternative to 'find'                            |
| [ffmpeg](https://ffmpeg.org/)                                                         | A complete, cross-platform solution to record, convert and stream audio and video |
| [fzf](https://github.com/junegunn/fzf) + [PSFzf](https://github.com/kelleyma49/PSFzf) | A command-line fuzzy finder / A PowerShell wrapper around the fuzzy finder fzf    |
| [Nerd Fonts](https://www.nerdfonts.com/)                                              | Iconic font aggregator, collection, and patcher                                   |
| [Oh My Posh](https://ohmyposh.dev/)                                                   | A blazing fast cross platform/shell prompt renderer                               |
| [restic](https://restic.net/)                                                         | Restic is a modern backup program that can back up your files                     |
| [Terminal-Icons](https://github.com/devblackops/Terminal-Icons)                       | A PowerShell module to show file and folder icons in the terminal                 |
| [yt-dlp](https://github.com/yt-dlp/yt-dlp)                                            | A youtube-dl fork with additional features and fixes                              |
| [zoxide](https://github.com/ajeetdsouza/zoxide)                                       | A smarter cd command. Supports all major shells                                   |

## License 📜

- Licensed under [GPLv3](LICENSE)

---

_Repository icon created with [Emblem](https://gitlab.gnome.org/World/design/emblem) and Windows icon from [Wikimedia](https://commons.wikimedia.org/wiki/File:Windows_logo_-_2012.svg)._
