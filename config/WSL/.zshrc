#          _
#  _______| |__
# |_  / __| '_ \
#  / /\__ \ | | |
# /___|___/_| |_|

# zsh-autocomplete plugin. Must be on top of zshrc
#source ~/.oh-my-zsh/custom/plugins/zsh-autocomplete/zsh-autocomplete.plugin.zsh

# zsh-syntax-higlighting theme
#source ~/.oh-my-zsh/custom/themes/Catppuccin/zsh-syntax-highlighting/themes/catppuccin_mocha-zsh-syntax-highlighting.zsh
source ~/.oh-my-zsh/custom/themes/Dracula/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# zsh-syntax-higlighting settings
ZSH_HIGHLIGHT_HIGHLIGHTERS=(main cursor)
typeset -gA ZSH_HIGHLIGHT_STYLES

# zsh-completions plugin
fpath+=${ZSH_CUSTOM:-${ZSH:-~/.oh-my-zsh}/custom}/plugins/zsh-completions/src

# Autocomplete
autoload -U compinit; compinit

# Nix directory PATH
export PATH="$HOME/.nix-profile/bin:$PATH"

# Enable Powerlevel10k instant prompt. Should stay close to the top of zshrc.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" > /dev/null
  # > /dev/null is a workaround for % character pop up on top of the terminal (disable instant prompt)
fi

source ~/.oh-my-zsh/custom/themes/powerlevel10k/powerlevel10k.zsh-theme

# Support for 256 colors
export TERM="xterm-256color"

# Workaround for lvim command
export PATH="$HOME/.local/bin:$PATH"

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

# EDITOR use lvim
export EDITOR='lvim'

# Update automatically
zstyle ':omz:update' mode auto
zstyle ':omz:update' frequency 1

# Load theme
ZSH_THEME="powerlevel10k/powerlevel10k"

# Plugins list
plugins=(fd git fzf tmux sudo zsh-autosuggestions zsh-completions zsh-syntax-highlighting zoxide)

source $ZSH/oh-my-zsh.sh

##### Alias list #####

# Arch
alias btw='neofetch'                                                                    # Neofecth command
alias pacman='sudo pacman'                                                              # Use sudo with pacman command
alias pacmanin='sudo pacman -S'                                                         # Pacman install command
alias pacmanrm='sudo pacman -Rsc'                                                       # Pacman remove package command
alias pacmanupg='sudo pacman -Syyu && flatpak update -y && flatpak uninstall --unused'  # Arch / Flatpak update command
alias paru='yay'									# Use yay instead of paru
alias yaourt='yay'                                                                      # Use yay instead of yaourt
alias yayin='yay -S'                                                                    # Yay install command

# Ubuntu
# alias aptin='sudo apt install'                                                                                                                                    # Apt install command
# alias aptrm='sudo apt --purge remove'                                                                                                                             # Apt remove command
# alias aptupg='sudo apt update && sudo apt full-upgrade -y && sudo apt autoremove -y && sudo apt autoclean && flatpak update -y && flatpak uninstall --unused'     # Full system update command

# Nix
alias nixgc='nix-collect-garbage -d'                            # Reclaim disk space
alias nixin='nix-env -iA'                                       # Install nixpkgs
alias nixs='nix search'                                         # Search for packages
alias nixsh='nix-shell'                                         # Create a new nix shell
alias nixun='nix-env --uninstall'                               # Uninstall nixpkgs
alias nixup='nix-channel --update && nix-env -u'                # Update Nix channels & installed packages

# Distrobox
#alias dboxc='distrobox-create'                                  # Create the distrobox
#alias dboxe='distrobox-enter'                                   # Enter the distrobox
#alias dboxex='distrobox-export'                                 # Application and service exporting
#alias dboxi='distrobox-init'                                    # Init the distrobox
#alias dboxl='distrobox-list'                                    # List available containers
#alias dboxrm='distrobox-rm'                                     # Remove containers
#alias dboxs='distrobox-stop'                                    # Stop containers

# System tools
alias cd='z'                                                    # Change cd to zoxide
alias burn='rm -rfi'                                            # Force delete command
alias cat='bat'                                                 # Change cat to bat
alias code='codium'						# Replace VSCode with VSCodium
alias df='duf'                                                  # Change df to duf
alias egrep='egrep --color=auto'                                # Add color to egrep output
alias fgrep='fgrep --color=auto'                                # Add color to fgrep output
alias find='fd'                                                 # Change find to fd
alias fm='xdg-open .'                                           # Open file manager (default one) in current directory
alias grep='grep --color=auto'                                  # Add color to grep output
alias l.='exa -a | egrep "^\."'                                 # List all hidden files
alias la='exa -al --color=always --group-directories-first'     # List all files and directories
alias ls='exa -a --color=always --group-directories-first'      # Change default ls to exa
alias mkdir='mkdir -pv'                                         # Create parent folder
alias reload='source ~/.zshrc'                                  # Reloads the zshrc file
alias rr='ranger'                                               # Ranger file manager command
alias sudo='sudo '                                              # Sudo respect aliases
alias top='htop'                                                # Change top to htop
alias vi='lvim'                                                 # Change vi to Neovim (lvim)
alias vim='lvim'                                                # Change vim to Neovim (lvim)

# yt-dlp
alias yt-besta='yt-dlp --extract-audio --audio-format best'     # Best audio quality
alias yt-bestv='yt-dlp -f bestvideo+bestaudio'                  # Best video quality
alias yt-mp3='yt-dlp --extract-audio --audio-format mp3'        # Extract audio only (mp3)

# Play audio / video files
alias playavi='mpv *.avi'                                       # Play all avi files from a directory
alias playmkv='mpv *.mkv'                                       # Play all mkv files from a directory
alias playmp3='mpv *.mp3'                                       # Play all mp3 files from a directory
alias playmp4='mpv *.mp4'                                       # Play all mp4 files from a directory
alias playwav='mpv *.wav'                                       # Play all wav files from a directory

# LazyGit
alias lg='lazygit'                                              # Default command

# LazyDocker
alias ld='lazydocker'                                           # Default command

# LunarVim
alias lv='lvim'                                                 # Default command

# jp2a
alias ascii='jp2a --output=photo.txt --colors'                  # Create ascii art with colors from photos

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

# GPG git
export GPG_TTY=$TTY

##### Applications custom settings #####

# Better history search
export HISTFILESIZE=100000
export HISTSIZE=100000
export SAVEHIST=100000
setopt HIST_EXPIRE_DUPS_FIRST
setopt HIST_FIND_NO_DUPS
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_SPACE

# fzf
export FZF_DEFAULT_COMMAND="fd --type f --hidden --follow --exclude .git"
export FZF_ALT_C_OPTS="--ansi --preview-window 'right:70%' --preview 'tree -C {} | head -600'"
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
export FZF_CTRL_T_OPTS="--ansi --preview-window 'right:50%' --preview 'bat --color=always --line-range :700 {}'"

# Dracula colorscheme
export FZF_DEFAULT_OPTS="--height 70% --border --inline-info \
--color=fg:#f8f8f2,bg:#282a36,hl:#bd93f9 \
--color=fg+:#f8f8f2,bg+:#44475a,hl+:#bd93f9 \
--color=info:#ffb86c,prompt:#50fa7b,pointer:#ff79c6 \
--color=marker:#ff79c6,spinner:#ffb86c,header:#6272a4"

# Catppuccin colorscheme
#export FZF_DEFAULT_OPTS=" --height 70% --border --inline-info \
#--color=bg+:#313244,bg:#1e1e2e,spinner:#f5e0dc,hl:#f38ba8 \
#--color=fg:#cdd6f4,header:#f38ba8,info:#cba6f7,pointer:#f5e0dc \
#--color=marker:#f5e0dc,fg+:#cdd6f4,prompt:#cba6f7,hl+:#f38ba8"

# tldr python client theme
export TLDR_COLOR_NAME="magenta"
export TLDR_COLOR_DESCRIPTION="cyan"
export TLDR_COLOR_EXAMPLE="green"
export TLDR_COLOR_COMMAND="magenta"
export TLDR_COLOR_PARAMETER="yellow"

# bat
export MANPAGER="sh -c 'col -bx | bat -l man -p'" # bat as manpager
export BAT_THEME="Dracula"                        # bat theme Dracula
#export BAT_THEME="Catppuccin-mocha"               # bat theme Catppuccin-mocha

# Zoxide init
eval "$(zoxide init zsh)"

# The Fuck alias
eval "$(thefuck --alias)"

# Pipx autocomplete
eval "$(register-python-argcomplete pipx)"

# npm
export PATH="$PATH:$HOME/.node/bin"

# dotnet
export DOTNET_CLI_TELEMETRY_OPTOUT=1
export PATH="$PATH:$HOME/.dotnet/tools"
