# My custom PowerShell profile

# Set PowerShell output to UTF-8

[Console]::OutputEncoding = [Text.UTF8Encoding]::UTF8

# Obtain windows username

$username = [System.Environment]::UserName

# oh-my-posh setup

oh-my-posh init pwsh --config "C:\Users\$username\AppData\Local\Programs\oh-my-posh\themes\larserikfinholt.omp.json" | Invoke-Expression

# PATH

$env:Path = $env:Path + ";C:\Users\$username\Documents\Terminal_Apps\bat"
$env:Path = $env:Path + ";C:\Users\$username\Documents\Terminal_Apps\fd"
$env:Path = $env:Path + ";C:\Users\$username\Documents\Terminal_Apps\ffmpeg"
$env:Path = $env:Path + ";C:\Users\$username\Documents\Terminal_Apps\fzf"
$env:Path = $env:Path + ";C:\Users\$username\Documents\Terminal_Apps\yt-dlp"
$env:Path = $env:Path + ";C:\Users\$username\Documents\Terminal_Apps\zoxide"

# Import modules

Import-Module -Name Terminal-Icons
Import-Module PSReadLine
Import-Module PSFzf

# PSReadLine Settings

Set-PSReadLineOption -BellStyle None
Set-PSReadLineOption -PredictionSource History

# Shows navigable menu of all options when hitting Tab

Set-PSReadlineKeyHandler -Key Tab -Function MenuComplete

# Replace 'Ctrl+t' and 'Ctrl+r' with your preferred bindings

Set-PsFzfOption -PSReadlineChordProvider 'Ctrl+t' -PSReadlineChordReverseHistory 'Ctrl+r'

# Example command - use $Location with a different command

$commandOverride = [ScriptBlock]{ param($Location) Write-Host $Location }

# Pass your override to PSFzf

Set-PsFzfOption -AltCCommand $commandOverride

# Tab Expansion

#Set-PSReadLineKeyHandler -Key Tab -ScriptBlock { Invoke-FzfTabCompletion }

# fzf settings

$env:FZF_DEFAULT_COMMAND='fd --type file --follow --hidden --exclude .git'
$env:FZF_DEFAULT_COMMAND="fd --type file --color=always"
$env:FZF_CTRL_T_OPTS="--ansi --preview-window 'right:50%' --preview 'bat --color=always --line-range :700 {}'"

# Dracula colorscheme

$env:FZF_DEFAULT_OPTS="--height 70% --border --inline-info --color=fg:#f8f8f2,bg:#282a36,hl:#bd93f9 --color=fg+:#f8f8f2,bg+:#44475a,hl+:#bd93f9 --color=info:#ffb86c,prompt:#50fa7b,pointer:#ff79c6 --color=marker:#ff79c6,spinner:#ffb86c,header:#6272a4"

# yt-dlp functions

# Extract audio to mp3 format

function yt-mp3 {
    & "yt-dlp" "--extract-audio" "--audio-format" "mp3" $args
}

# Extract video at the best quality available

function yt-bestv {
    & "yt-dlp" "-f" "bestvideo+bestaudio" $args
}

# Extract audio at the best quality available

function yt-besta {
    & "yt-dlp" "--extract-audio" "--audio-format" "best" $args
}

# Zoxide setup

Invoke-Expression (& {
    $hook = if ($PSVersionTable.PSVersion.Major -lt 6) { 'prompt' } else { 'pwd' }
    (zoxide init --hook $hook powershell | Out-String)
})